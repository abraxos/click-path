from pathlib import Path
from typing import Iterable

from pytest import mark
from typeguard import typechecked
import click
from click.testing import CliRunner

from click_path import GlobPaths


THIS_FILE = Path(__file__)
EXISTING_READABLE_FILES = GlobPaths(files_okay=True,
                                    dirs_okay=False,
                                    writable_only=False,
                                    readable_only=True,
                                    resolve=True,
                                    at_least_one=True)
WRITABLE_FILES = GlobPaths(files_okay=True,
                           dirs_okay=False,
                           writable_only=True,
                           readable_only=False,
                           resolve=False,
                           at_least_one=True)
READABLE_FILES = GlobPaths(files_okay=True,
                           dirs_okay=False,
                           writable_only=False,
                           readable_only=True,
                           resolve=False,
                           at_least_one=True)
ALL_TRUE = GlobPaths(files_okay=True,
                     dirs_okay=True,
                     writable_only=True,
                     readable_only=True,
                     resolve=True,
                     at_least_one=True)
ALL_FALSE = GlobPaths(files_okay=False,
                      dirs_okay=False,
                      writable_only=False,
                      readable_only=False,
                      resolve=False,
                      at_least_one=False)


@mark.parametrize("glob_type, pattern, exp_code, exp_out", [
    [EXISTING_READABLE_FILES, str(THIS_FILE), 0, str(THIS_FILE)],
    [EXISTING_READABLE_FILES, str(THIS_FILE / 'not real'), 2, 'No paths from'],
    [EXISTING_READABLE_FILES, str(THIS_FILE.parent), 2, 'No paths from'],
    [EXISTING_READABLE_FILES, str(THIS_FILE.parent) + '/*', 0, str(THIS_FILE)],
    [EXISTING_READABLE_FILES, '/root/.ssh', 2, 'No paths from'],
    [EXISTING_READABLE_FILES, '/etc/sudoers', 2, 'No paths from'],
    [EXISTING_READABLE_FILES, '/etc/sudoers', 2, 'No paths from'],
    [EXISTING_READABLE_FILES, str(THIS_FILE), 0, str(THIS_FILE)],
    [WRITABLE_FILES, str(THIS_FILE.parent) + '/*', 0, str(THIS_FILE)],
    [READABLE_FILES, str(THIS_FILE.parent) + '/*', 0, str(THIS_FILE)],
    [READABLE_FILES, str(THIS_FILE.parent / 'not real'), 2, 'No paths from'],
])
@typechecked
def test_glob_paths(glob_type: GlobPaths,
                    pattern: str,
                    exp_code: int,
                    exp_out: str) -> None:
    @click.command()
    @click.option('-t', '--tests', type=glob_type)
    @typechecked
    def cli(tests: Iterable[Path]) -> None:
        click.echo(str(tests))

    runner = CliRunner()
    result = runner.invoke(cli, ['--tests', pattern])
    assert result.exit_code == exp_code
    assert exp_out in result.output

    
@typechecked
def test_glob_path_fail_message() -> None:
    @click.command()
    @click.option('-t', '--tests', type=ALL_TRUE)
    @typechecked
    def cli(tests: Iterable[Path]) -> None:
        click.echo(str(tests))

    res = CliRunner().invoke(cli, ['--tests', '/file/that/does/not/exist'])
    print(res.output)
    assert '[files, directories, write-permissions, read-permissions]\n' in res.output

@typechecked
def test_glob_path_fail_message_all_false() -> None:
    @click.command()
    @click.option('-t', '--tests', type=ALL_FALSE)
    @typechecked
    def cli(tests: Iterable[Path]) -> None:
        click.echo(str(tests))

    res = CliRunner().invoke(cli, ['--tests', '/file/that/does/not/exist'])
    assert res.output.endswith('[]\n')

@typechecked
def test_glob_path_non_existent_readable() -> None:
    @click.command()
    @click.option('-t', '--tests', type=EXISTING_READABLE_FILES)
    @typechecked
    def cli(tests: Iterable[Path]) -> None:
        click.echo(str(tests))

    res = CliRunner().invoke(cli, ['--tests', str(THIS_FILE.parent / 'stuff.txt')])
    print(res.output)
    assert 'stuff.txt' in res.output
