from pathlib import Path

from pytest import mark
from typeguard import typechecked
import click
from click.testing import CliRunner
from click import BaseCommand

from click_path import PathlibPath


THIS_FILE = Path(__file__)
EXISTING_READABLE_FILE = PathlibPath(exists=True,
                                     file_okay=True,
                                     dir_okay=False,
                                     writable=False,
                                     readable=True,
                                     resolve_path=True)
EXISTING_WRITABLE_FILE = PathlibPath(exists=True,
                                     file_okay=True,
                                     dir_okay=False,
                                     writable=True,
                                     readable=False,
                                     resolve_path=True)
EXISTING_WRITABLE_DIR = PathlibPath(exists=True,
                                    file_okay=False,
                                    dir_okay=True,
                                    writable=True,
                                    readable=False,
                                    resolve_path=True)
NONEXISTENT_WRITABLE_FILE = PathlibPath(exists=False,
                                        file_okay=True,
                                        dir_okay=False,
                                        writable=True,
                                        readable=False,
                                        resolve_path=False)


@click.command()
@click.option('-t', '--test', type=EXISTING_READABLE_FILE)
@typechecked
def cli_existing_readable_file(test: Path):
    click.echo(str(test))


@click.command()
@click.option('-t', '--test', type=EXISTING_WRITABLE_FILE)
@typechecked
def cli_existing_writable_file(test: Path):
    click.echo(str(test))


@click.command()
@click.option('-t', '--test', type=NONEXISTENT_WRITABLE_FILE)
@typechecked
def cli_nonexistent_writable_file(test: Path):
    click.echo(str(test))


@click.command()
@click.option('-t', '--test', type=EXISTING_WRITABLE_DIR)
@typechecked
def cli_existing_writable_dir(test: Path):
    click.echo(str(test))


@mark.parametrize("cli, path, exp_code, exp_out", [
    [cli_existing_readable_file, THIS_FILE, 0, str(THIS_FILE)],
    [cli_existing_readable_file, THIS_FILE / 'not real', 2, 'does not exist'],
    [cli_existing_readable_file, THIS_FILE.parent, 2, 'is a directory'],
    [cli_existing_readable_file, Path('/root/.ssh'), 2, 'Permission denied'],
    [cli_existing_readable_file, Path('/etc/sudoers'), 2, 'not readable'],
    [cli_existing_writable_file, Path('/etc/sudoers'), 2, 'not writable'],
    [cli_existing_writable_file, THIS_FILE, 0, str(THIS_FILE)],
    [cli_nonexistent_writable_file, Path('/etc/sudoers'), 2, 'not writable'],
    [cli_nonexistent_writable_file, THIS_FILE, 0, str(THIS_FILE)],
    [cli_existing_writable_dir, THIS_FILE, 2, 'is a file'],
])
@typechecked
def test_pathlib_path(cli: BaseCommand,
                      path: Path,
                      exp_code: int,
                      exp_out: str) -> None:
    runner = CliRunner()
    result = runner.invoke(cli, ['--test', str(path)])
    print(result.output)
    assert result.exit_code == exp_code
    assert exp_out in result.output
